﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public int pooledAmount = 20;
    public bool willGrow = true;
    public GameObject bullet;
    private List<GameObject> pooledObjects;
    public int size;

    // Use this for initialization
    private void Start()
    {
        pooledObjects = new List<GameObject>();
        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = Instantiate(bullet);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
    }

    //Obtener pool de objetos
    public GameObject GetPooledObject()
    {
        size = pooledObjects.Count;
        //Revisa Hierachy por objetos
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }

        if (willGrow)
        {
            GameObject obj = Instantiate(bullet);
            pooledObjects.Add(obj);
            return obj;
        }

        return null;
    }
}