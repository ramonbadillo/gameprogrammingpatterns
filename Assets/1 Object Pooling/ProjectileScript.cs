﻿using System.Collections;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    public float speed;

    private void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
    }
}