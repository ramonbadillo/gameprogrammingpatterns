﻿using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject shot;
    public float fireRate;
    public GameObject soldierPrefab;
    public int soldierNumber;
    private static bool uSE_POOL;
    public bool usePool;
    private List<GameObject> soldierList = new List<GameObject>();
    private float nextFire;
    private ObjectPooler objectPooler;

    public static bool USE_POOL
    {
        get
        {
            return uSE_POOL;
        }

        set
        {
            uSE_POOL = value;
        }
    }

    private void Awake()
    {
        USE_POOL = usePool;
    }

    private void Start()
    {
        objectPooler = GetComponent<ObjectPooler>();

        for (int i = 0; i < soldierNumber; i++)
        {
            GameObject soldier = Instantiate(soldierPrefab, new Vector3(i * 3, 0, 0), Quaternion.identity) as GameObject;
            soldierList.Add(soldier);
        }
    }

    private void Update()
    {
        if (Time.time > nextFire)
        {
            for (int i = 0; i < soldierList.Count; i++)
            {
                nextFire = Time.time + fireRate;
                if (usePool)
                {
                    GameObject bullet = objectPooler.GetPooledObject();
                    bullet.transform.position = soldierList[i].transform.position;
                    bullet.SetActive(true);
                }
                else
                {
                    Instantiate(shot, soldierList[i].transform.position, soldierList[i].transform.rotation);
                }
            }
        }
    }
}