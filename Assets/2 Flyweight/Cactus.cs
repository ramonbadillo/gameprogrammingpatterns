﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cactus
{
    public List<Vector3> position;
    public List<Vector3> rotation;
    public List<Vector3> scale;
    public Mesh cactusMesh;
}