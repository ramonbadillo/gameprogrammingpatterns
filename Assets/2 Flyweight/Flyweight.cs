﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flyweight : MonoBehaviour
{
    public bool useFlyweigth = true;

    public List<Mesh> cactusMesh;
    public List<Material> cactusMaterial;
    private GameObject cactusFlyweigth;

    public List<GameObject> cactusPrefabs;

    public int objectNumber;

    private int sharedMaterialIndex = 0;

    private void Awake()
    {
        if (useFlyweigth)
        {
            foreach (Mesh actualMesh in cactusMesh)
            {
                cactusFlyweigth = new GameObject("cactus");
                //cactus.gameObject.isStatic = true;
                MeshFilter meshFilter = cactusFlyweigth.AddComponent<MeshFilter>();
                MeshRenderer meshRender = cactusFlyweigth.AddComponent<MeshRenderer>();
                //Cactus cactusClass = cactus.AddComponent<Cactus>();
                //meshFilter.mesh = cactusMesh;
                //meshRender.material = cactusMaterial;
                int randomObject = Random.Range(0, cactusMesh.Count);
                meshFilter.sharedMesh = actualMesh;
                meshRender.sharedMaterial = cactusMaterial[sharedMaterialIndex];
                cactusFlyweigth.isStatic = true;
                for (int i = 0; i < objectNumber / cactusMesh.Count; i++)
                {
                    float halfMapSize = 100f;

                    float x = Random.Range(-halfMapSize, halfMapSize);
                    float z = Random.Range(-halfMapSize, halfMapSize);

                    Vector3 cactusPos = new Vector3(x, 0f, z);

                    GameObject newCactus = Instantiate(cactusFlyweigth, cactusPos, Quaternion.identity) as GameObject;
                    //newCactus.isStatic = true;
                    //cactusFlyweigth = null;
                }
                sharedMaterialIndex++;
                cactusFlyweigth = null;
            }
        }
    }

    // Use this for initialization
    private void Start()
    {
        if (!useFlyweigth)
        {
            for (int i = 0; i < objectNumber; i++)
            {
                //Generate random map coordinates
                float halfMapSize = 100f;

                float x = Random.Range(-halfMapSize, halfMapSize);
                float z = Random.Range(-halfMapSize, halfMapSize);

                Vector3 cactusPos = new Vector3(x, 0f, z);

                //Create a tree
                GameObject newCactus = Instantiate(cactusPrefabs[Random.Range(0, cactusPrefabs.Count)], cactusPos, Quaternion.identity) as GameObject;
            }
        }
    }

    private List<Vector3> privategetVector3()
    {
        //Create a new list
        List<Vector3> bodyPartPositions = new List<Vector3>();

        //Add body part positions to the list
        for (int i = 0; i < 1000; i++)
        {
            bodyPartPositions.Add(new Vector3());
        }

        return bodyPartPositions;
    }
}