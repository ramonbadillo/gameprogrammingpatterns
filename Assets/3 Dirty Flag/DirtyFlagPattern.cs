﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtyFlagPattern : MonoBehaviour
{
    public GameObject Sergeant;
    public int squads;
    public int rows = 10;
    public int columns = 10;
    public bool ActivatePattern = true;

    // Use this for initialization
    private void Start()
    {
        for (int x = 0; x < squads; x++)
        {
            GameObject sergeantObject = Instantiate(Sergeant, new Vector3(transform.position.x + (x * rows * 3), 0, 0), Quaternion.identity) as GameObject;

            sergeantObject.GetComponent<Squad>().ActivatePattern = ActivatePattern;
            sergeantObject.GetComponent<Squad>().rows = rows;
            sergeantObject.GetComponent<Squad>().columns = columns;


        }
    }

    // Update is called once per frame
    private void Update()
    {
    }
}