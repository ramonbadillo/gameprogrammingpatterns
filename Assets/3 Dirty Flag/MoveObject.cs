﻿using System.Collections;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    private Rigidbody rb;
    public Vector3 direction = Vector3.forward;
    public float speed = 2.0f;

    // Use this for initialization
    private void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    private void Update()
    {
        rb.velocity = direction * speed;
    }
}