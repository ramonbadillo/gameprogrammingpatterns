﻿using System.Collections.Generic;
using UnityEngine;

public class Squad : MonoBehaviour
{
    public GameObject Child;
    public int rows = 10;
    public int columns = 10;
    public bool ActivatePattern = true;

    //private List<GameObject> allChilds = new List<GameObject>();
    private Rigidbody rigidParent;

    // Use this for initialization
    private void Start()
    {
        rigidParent = gameObject.GetComponent<Rigidbody>();
        for (int x = 1; x < rows; x++)
        {
            for (int z = 1; z < columns; z++)
            {
                GameObject pawnObject = Instantiate(Child, new Vector3(transform.position.x + (x * 3), 0, transform.position.z + (z * 3)), Quaternion.identity) as GameObject;

                if (ActivatePattern)
                {
                    pawnObject.transform.SetParent(gameObject.transform);
                }
                else
                {
                    Rigidbody rigid = pawnObject.AddComponent<Rigidbody>();
                    rigid.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
                    MoveObject move = pawnObject.AddComponent<MoveObject>();
                }

                rigidParent.velocity = Vector3.forward * 2;
            }
        }
    }

    // Update is called once per frame
    private void Update()
    {
    }
}