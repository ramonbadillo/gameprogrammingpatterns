﻿using UnityEngine;

public class PenaMonoBehavior : MonoBehaviour
{
    private float fleeSpeed = 10f;
    private float strollSpeed = 1f;
    private float attackSpeed = 5f;

    private float health = 100f;
    private Transform enemyObj;
    public Transform playerObj;

    // Use this for initialization
    private void Start()
    {
        enemyObj = gameObject.transform;
    }

    // Update is called once per frame
    private void Update()
    {
        float distance = (enemyObj.position - playerObj.position).magnitude;

        if (health < 20f)
        {
            enemyObj.rotation = Quaternion.LookRotation(enemyObj.position - playerObj.position);
            //Move
            enemyObj.Translate(enemyObj.forward * fleeSpeed * Time.deltaTime);
        }
        else if (distance > 2f)
        {
            enemyObj.LookAt(playerObj.transform);
            //enemyObj.rotation = Quaternion.LookRotation(playerObj.position - enemyObj.position);
            //Move
            enemyObj.Translate(enemyObj.forward * attackSpeed * Time.deltaTime);
        }

        if (health > 60f)
        {
            Vector3 randomPos = new Vector3(Random.Range(0f, 100f), 0f, Random.Range(0f, 100f));
            enemyObj.rotation = Quaternion.LookRotation(enemyObj.position - randomPos);
            //Move
            enemyObj.Translate(enemyObj.forward * strollSpeed * Time.deltaTime);
        }

        if (distance < 10f)
        {
            enemyObj.LookAt(playerObj.transform);
            //enemyObj.rotation = Quaternion.LookRotation(playerObj.position - enemyObj.position);
            //Move
            enemyObj.Translate(enemyObj.forward * attackSpeed * Time.deltaTime);
        }

        //The skeleton has bow and arrow so can attack from distance
        if (distance < 5f)
        {
            Debug.Log("Attacking");
        }
        else if (distance > 15f)
        {
            Vector3 randomPos = new Vector3(Random.Range(0f, 100f), 0f, Random.Range(0f, 100f));
            enemyObj.rotation = Quaternion.LookRotation(enemyObj.position - randomPos);
            //Move
            enemyObj.Translate(enemyObj.forward * strollSpeed * Time.deltaTime);
        }
    }
}