﻿using System.Collections.Generic;
using UnityEngine;

namespace StatePattern
{
    public class StatePattern : MonoBehaviour
    {
        public bool statePattern;
        public static bool STATE_PATTERN;
        public GameObject playerObj;
        public GameObject pejezombieObj;
        public GameObject penabotObj;
        public Transform target;


        public float orbitDistance = 10.0f;
        public float orbitDegreesPerSec = 180.0f;

        //A list that will hold all enemies
        private List<Enemy> enemies = new List<Enemy>();

        void Awake()
        {
            STATE_PATTERN = statePattern;
        }

        // Use this for initialization
        private void Start()
        {
            //Add the enemies we have
            enemies.Add(new Pejezombie(pejezombieObj.transform));
            enemies.Add(new Penabot(penabotObj.transform));
        }

        // Update is called once per frame
        private void Update()
        {
            //Update all enemies to see if they should change state and move/attack player
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].UpdateEnemy(playerObj.transform);
            }
        }

        private void LateUpdate()
        {
            Orbit();

        }

        
        void Orbit()
        {
            if (target != null)
            {
                // Keep us at orbitDistance from target
                playerObj.transform.position = target.position + (playerObj.transform.position - target.position).normalized * orbitDistance;
                playerObj.transform.RotateAround(target.position, Vector3.up, orbitDegreesPerSec * Time.deltaTime);
            }
        }
    }
}