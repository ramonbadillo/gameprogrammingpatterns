﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace CommandPattern
{

    public class InputHandler : MonoBehaviour
    {

        public float moveDistance = 10.0f;
        public GameObject objectToMove;

        private MoveCommandReciver moveCommandReciver;
        private List<MoveCommand> commands = new List<MoveCommand>();
        private int currentCommandIndex = 0;


        // Use this for initialization
        void Start()
        {
            moveCommandReciver = new MoveCommandReciver();


        }

        public void Undo()
        {
            if (currentCommandIndex > 0)
            {
                currentCommandIndex--;
                MoveCommand moveCommand = commands[currentCommandIndex];
                moveCommand.UnExecute();
            }
        }

        public void Redo()
        {
            if (currentCommandIndex < commands.Count)
            {
                MoveCommand moveCommand = commands[currentCommandIndex];
                currentCommandIndex++;
                moveCommand.Execute();
            }
        }

        private void Move(MoveDirection direction)
        {
            MoveCommand moveCommand = new MoveCommand(moveCommandReciver, direction, moveDistance, objectToMove);
            moveCommand.Execute();
            commands.Add(moveCommand);
            currentCommandIndex++;
        }


        //Simple move commands to attach to UI buttons
        public void MoveUp() { Move(MoveDirection.up); }
        public void MoveDown() { Move(MoveDirection.down); }
        public void MoveLeft() { Move(MoveDirection.left); }
        public void MoveRight() { Move(MoveDirection.right); }


    }
}