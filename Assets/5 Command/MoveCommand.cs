﻿using UnityEngine;

namespace CommandPattern
{
    public enum MoveDirection { up, down, left, right };

    public class MoveCommand
    {
        private MoveDirection _direction;
        private MoveCommandReciver _reciver;
        private float _distance;
        private GameObject _gameObject;

        public MoveCommand(MoveCommandReciver reciver, MoveDirection direcrion, float distance, GameObject objectToMove)
        {
            this._reciver = reciver;
            this._direction = direcrion;
            this._distance = distance;
            this._gameObject = objectToMove;
        }

        public void Execute()
        {
            _reciver.MoveOperation(_gameObject,_direction,_distance);
        }

        public void UnExecute()
        {
            _reciver.MoveOperation(_gameObject, InverseDirection(_direction), _distance);
        }

        private MoveDirection InverseDirection(MoveDirection direction)
        {
            switch (direction)
            {
                case MoveDirection.up:
                    return MoveDirection.down;

                case MoveDirection.down:
                    return MoveDirection.up;

                case MoveDirection.left:
                    return MoveDirection.right;

                case MoveDirection.right:
                    return MoveDirection.left;

                default:
                    Debug.LogError("Unknown MoveDirection");
                    return MoveDirection.up;
            }
        }
    }
}