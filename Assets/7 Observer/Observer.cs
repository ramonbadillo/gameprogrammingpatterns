﻿using System;
using System.Collections;
using UnityEngine;

public class Observer : MonoBehaviour
{
    private Subject _subject;

    // Use this for initialization
    private void Start()
    {
        _subject.AddObserver += TurnOffAudio;
    }

    private void TurnOffAudio()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }
}