﻿using System;
using System.Collections;
using UnityEngine;

public class Subject : MonoBehaviour
{
    public Action AddObserver;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
    }

    public void DoSomeAction()
    {
        AddObserver();
    }
}